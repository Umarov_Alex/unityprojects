﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnakeMovement : MonoBehaviour {

    public float Speed;
    public float RotSpeed;
    public float Zoffset = -0.5f;    
    public List<GameObject> tailObj= new List<GameObject>();
    public GameObject TailPrefab;
    public Text ScoreText;
    public int score=0;

    void Start () {
        tailObj.Add(gameObject);
    }
	
	void Update () {
        ScoreText.text = "Score - "+ score.ToString();
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
        if(Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.up * RotSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.down * RotSpeed * Time.deltaTime);
        }       
    }

    public void AddTail()
    {
        score++;
        Speed = Speed + 0.5f;
        Vector3 newTailPos = tailObj[tailObj.Count-1].transform.position;
        newTailPos.z -= Zoffset;
        tailObj.Add(GameObject.Instantiate(TailPrefab, newTailPos, Quaternion.identity) as GameObject);
    }
}
